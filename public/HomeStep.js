$(document).ready(function() {
    var currentGfgStep, nextGfgStep, previousGfgStep;
    var opacity;
    var current = 1;
    var steps = $("fieldset").length;

    var stepInputs = {
        "1": [],
        "2": ["first_name", "birthday", "place_of_birth", "email", "phone", "zip", "address"],
        "3": [],
        "4": questionRequest,
        "5": ["existing_area", "projected_area"],
        "6": []
    }

    setProgressBar(current);

    $(".next-btn").click(function() {

        currentGfgStep = $(this).parent();
        nextGfgStep = $(this).parent().next();

        if (validateStep(current)) {
            $('html,body').scrollTop(0);
            $("#progressbar li").eq($("fieldset")
                .index(nextGfgStep)).addClass("active");

            nextGfgStep.show();
            currentGfgStep.animate({ opacity: 0 }, {
                step: function(now) {
                    opacity = 1 - now;

                    currentGfgStep.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    nextGfgStep.css({ 'opacity': opacity });
                },
                duration: 500
            });
            setProgressBar(++current);
        }

    });

    $(".prev-btn").click(function() {

        currentGfgStep = $(this).parent();
        previousGfgStep = $(this).parent().prev();

        $("#progressbar li").eq($("fieldset")
            .index(currentGfgStep)).removeClass("active");

        previousGfgStep.show();

        currentGfgStep.animate({ opacity: 0 }, {
            step: function(now) {
                opacity = 1 - now;

                currentGfgStep.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previousGfgStep.css({ 'opacity': opacity });
            },
            duration: 500
        });
        setProgressBar(--current);
    });

    function setProgressBar(currentStep) {
        var percent = parseFloat(100 / steps) * current;
        percent = percent.toFixed();
        $(".progress-bar")
            .css("width", percent + "%")
    }

    $(".submit-btn").click(function() {
        // return false;
    })


    function validateStep(current) {
        var inputs = getKeyByValue(stepInputs, current);
        var isValid = true;
        var errors = [];
        if (current == 1 || current == 3) {
            if (!$("input[name=" + get_input_radio(current) + "]:checked").val()) {
                if ($(".check")) $(".check").remove();
                $("." + get_radio_show_error(current)).after(`<p style="font-size:13px; font-weight:600 ;text-align:left;margin-left:7%"><span  class="text-danger check">Veuillez sélectionner</span></p>`);
                isValid = false
            }
        } else {
            inputs.forEach(function(input) {
                var thisInput = $("#" + input);
                if (isEmpty(thisInput.val()) && isBlank(thisInput.val()) && thisInput.attr("required")) {
                    errors.push(input)
                    if (!$("#error-on-" + thisInput.attr("id")).length) {
                        thisInput.after(showError(thisInput.attr("id")))
                    }
                    isValid = false
                } else if (!isEmail($.trim($("input[name='email']").val()))) {
                    if ($(".check_email")) $(".check_email").remove();
                    $("#email").after(`<span style="font-size:13px;font-weight:600" class="text-danger check_email">Veuillez insérer un e-mail valide! </span>`);
                    isValid = false
                }
            });
        }
        return isValid;
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function getKeyByValue(object, value) {
        return object[value];
    }

    function isEmpty(str) {
        return (!str || str.length === 0);
    }

    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }

    function showError(input) {
        return '<span style="font-size:13px;font-weight:600" id="error-on-' + input + '" class="errors text-danger"> Ce champ est obligatoire </span>'
    }

    function hideError(input) {
        $("#error-on-" + input).remove()
    }


    function get_input_radio(current) {
        var input_name = "client_type";
        if (current == 3) input_name = "'categorie[]'"
        return input_name;
    }

    function get_radio_show_error(current) {
        var error = "type";
        if (current == 3) error = "categorie"
        return error;
    }

    $(".form-control").on("keyup change", function() {
        var id = $(this).attr("id");
        if ($("#error-on-" + id).length) {
            hideError(id)
        }
    })

    $("#email").on("keyup change", function() {
        $('.check_email').remove()
    })

    $("input[name='client_type'],input[name='categorie[]']").on("change", function() {
        $('.check').remove()
    })

    $("input[name='client_type']").change(function() {
        if ($("input[name='client_type']:checked").val() == "corporate") {
            stepInputs[2].push("company_name", "siret")
            $(".req").prop('required', true);
            $(".entreprise").css("display", "")
        } else {
            $(".req").prop('required', false);
            $(".entreprise").css("display", "none")
        }
    });

});




var max = 1;

function del(params) {
    if (max > 1) {
        max--
        close(params)
    }
}

function close(params) {
    $(params).closest('.file-input').remove()
}


function add(params) {
    if (max < 6) {
        max++
        clone(params)
    }

    function clone(params) {
        $("#" + params).clone().insertBefore("#" + params).find("input[type='file']").val("");
    }
}
