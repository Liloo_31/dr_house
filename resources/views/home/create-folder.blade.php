@extends('base.create')

@section('content')
    <div class="row" id="home">
        <div class="col-md-5" id="content" style="background-image: url({{ asset("/dr_house_img/dr_house_bg.png") }})">
        </div>
        <div class="col-md-7 align-middle">
            <div class="my-4 login" style="text-align: right">Vous avez déjà un compte? <a href="{{ url('/login') }}">S'identifier</a></div>
            <form action="{{ url("/home/request/save") }}" class="needs-validation" id="folder_form" method="POST" name="form-wrapper" novalidate="" enctype="multipart/form-data">
            @csrf
            <div class="container">
                <div id="form">
                    <ul id="progressbar">
                        <li></li>
                        <li id="step1"></li>
                        <li id="step2"></li>
                        <li id="step3"></li>
                        <li id="step4"></li>
                    </ul>
                    {{-- <div class="progress">
                        <div class="progress-bar"></div>
                    </div> <br> --}}
                    <fieldset style="padding-bottom: 20%">
                        <h2 class="title1" >Quel est votre profil?</h2>
                        @include('home.includes.client-type')
                        <button type="button" class="next-btn">Suivant</button>
                    </fieldset>
                    <fieldset>
                        <h2 class="title">Qui êtes-vous?</h2>
                        @include('home.includes.client-information')
                        <button type="button" class="prev-btn">Précédent</button>
                        <button type="button" class="next-btn">Suivant</button>
                    </fieldset>
                    <fieldset>
                        <h2 class="title">Choisissez vos projets</h2>
                        @include('home.includes.project-type')
                        <button type="button" class="prev-btn">Précédent</button>
                        <button type="button" class="next-btn">Suivant</button>
                    </fieldset>
                    <fieldset>
                        <h2 class="title">Parlez nous en détail de votre projet</h2>
                        @include('home.includes.project-information')
                        <button type="button" class="prev-btn">Précédent</button>
                        <button type="button" class="next-btn">Suivant</button>
                    </fieldset>
                    <fieldset>
                        <div class="finish">
                            <h2 class="title1">Inscrivez-vous pour recevoir le devis</h2>
                            <p class="final">Merci! nous avons bien reçu toutes les informations, vous recevrez une notification par mail quand le devis sera disponible d'ici 24h au maximum</p>
                        </div>
                        <button type="button" class="prev-btn">Précédent</button>
                        <button type="submit" class="submit-btn">Je valide <i class="bi bi-check-lg"></i></button>
                    </fieldset>
                </div>
            </div>
            </form>
        </div>
    </div>

@endsection
@section('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    @include('home.step-script',["questions" => json_encode($questions) ])
    <script>
        $(document).ready(function() {
            setInterval(deleteInvalidFeedback, 1);
            function deleteInvalidFeedback() {
                $(".invalid-feedback").remove();
            }

            $("#folder_form").appForm({
                isModal: false,
                showAlertSuccess: false,
                onSuccess: function(response) {
                    var icon = response.success ? "success" : "error"
                    Swal.fire({
                                    text: response.message,
                                    icon: icon,
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok",
                                    customClass: {
                                        confirmButton: "btn btn-secondary"
                                    }
                                }).then(function(result) {
                                    if (result.isConfirmed && response.success) {
                                        window.location.replace("{{ url("/") }}");
                                    }
                                });

                },
            });
            $("#folder_form").keypress(function(e) {
                if (e.which == 13) {
                    return false;
                }
            });


        })
        var questionRequest = new Array();
        @for ($i=0; $i<count($questions);$i++)
            questionRequest.push("question_"+{{ $i+1 }});
        @endfor

    </script>
@endsection
