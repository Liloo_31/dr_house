{{-- <div class="mt-1 my-4 require">
    <label class="form-label">Quel est la surface de placher créée par le projet ?</label>
    <input class="form-control" id="existing_area" required="required" data-rule-number="true" data-msg-number="Veuillez entrer un nombre" name="existing_area" type="number">
 </div>
 <div class="mt-1 my-4 require">
    <label class="form-label">Quel est la surface de placher des contructions existantes ?</label>
    <input class="form-control" id="projected_area" required="required" data-rule-number="true" data-msg-number="Veuillez entrer un nombre" name="projected_area" type="number">
 </div>
 <div class="mt-1 my-4">
     <label class="form-label">Avez-vous fait une analyse de votre projet par rapport aux règles d'urbanisme ?</label>
     <div class="form-check ps-0 q-box">
        <div class="q-box__question my-2">
           <input class="form-check-input question__input q-checkbox" id="analyse_1" name="analyse_urbanisme" type="radio" value="0">
           <label class="form-check-label question__label" for="analyse_1">Oui et mon projet passe</label>
        </div>
     </div>
     <div class="form-check ps-0 q-box my-2">
        <div class="q-box__question">
           <input checked class="form-check-input question__input" id="analyse_2" name="analyse_urbanisme" type="radio" value="1">
           <label class="form-check-label question__label" for="analyse_2">Non je veux que vous analysez mon projet par rapport au règlement</label>
        </div>
     </div>
 </div>
 <div class="mt-1 my-4">
     <label class="form-label">Pouvez-vous nous expliquez de manière précise votre projet ?</label>
     <textarea class="form-control" name="remark" id="exampleFormControlTextarea1" rows="2"></textarea>
 </div> --}}

@foreach ($questions as $question)
    {{-- <div class="fv-row mb-10 fv-plugins-icon-container">
        <label
            class="form-label required">{{ get_array_value($question, 'question') }}
        </label>
        <textarea placeholder="..." name="{{ get_array_value($question, 'name') }}"
            class="form-control form-control-lg form-control-solid"
            data-kt-autosize="true"
            rows="{{ get_array_value($question, 'rows') ?? '1' }}">
        </textarea>
        <div class="fv-plugins-message-container invalid-feedback"></div>
    </div> --}}
    <div class="mt-1 my-4 require questions">
        <label class="form-label required projectInfo">{{ get_array_value($question, 'question') }}</label>
        <input class="form-control" id="{{ get_array_value($question, 'name') }}" required="required" data-rule-number="true" name="{{ get_array_value($question, 'name') }}" type="text">
        {{-- <textarea class="form-control" name="{{ get_array_value($question, 'name') }}" data-kt-autosize="true" id="exampleFormControlTextarea1" rows="{{ get_array_value($question, 'rows') ?? '1' }}"></textarea> --}}
    </div>
@endforeach

<p class="file">Merci de télécharger ici tous les documents permettant de mieux comprendre votre projet
    <span>(esquisse-plan-photo-courrier de la mairie)</span>
</p>
<p class="link">Si vous n'êtes pas doué avec les dessins à la main, voici un outil pour aider
    <a href="https://www.kozikaza.com/">www.kozikaza.com</a>
</p>

<div class="mt-1" style="text-align: left; margin-left:4%">
    <label class="form-label" for="city" style="color: black">Fichier:
        <a href="javascript:;" onClick="add('file')">{{-- <i class="fa fa-plus mx-4 mr-0" aria-hidden="true"></i> --}}
            <span class="text-primary"><i class="bi bi-plus-square-dotted mx-4 fs-2 text-primary "></i></span></a>
    </label>
    <div class="row file-input" id="file">
        <div class="col-9 custom-file my-1">
            <input class="form-inputs form-inputs-sm" class="file" name="files[]" type="file">
        </div>
        <div class="col-2 my-2">
            <a href="javascript:;" onClick="del(this)">{{--<i class="ri-delete-bin-6-line mx-2 mr-0 text-danger" style="font-size:18px" aria-hidden="true"></i>  --}}
                <span class="text-danger"><i class="bi bi-trash mx-4 fs-4 text-danger "></i></span></a>
        </div>
    </div>
</div>



