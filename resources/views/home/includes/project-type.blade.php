<div class="row type_projet categorie">

    @foreach ($offers as $offer)
        @if ($offer->categories->count())
            <h4 class="fw-bolder projectType">* {{ $offer->name }} :
                @if ($offer->description)
                    <i class="fas fa-exclamation-circle ms-2 fs-7"
                        data-bs-toggle="tooltip" title=""
                        data-bs-original-title="{{ $offer->description }}">
                    </i>
                @endif
            </h4>
            <div class="mb-5 projectCategory">
                @foreach ($offer->categories as $categorie)
                    <div class="form-check ps-0 q-box mx-4">
                        <div class="q-box__question my-2">
                            <input class="form-check-input question__input q-checkbox" id="type_{{ $categorie->id }}" name="categorie[]" type="checkbox" value="{{ $categorie->id }}">
                            <label class="form-check-label question__label " for="type_{{ $categorie->id }}">{{ ucfirst($categorie->name) }}</label>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    @endforeach

</div>
