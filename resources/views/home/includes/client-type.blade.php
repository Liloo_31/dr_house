<div class="form-check ps-0 q-box type my-10 typeClient">
    <div class="q-box__question my-5">
        <input class="form-check-input question__input" id="typeClient_1" name="client_type" type="radio" value="particular">
        <label class="form-check-label question__label_typeClient" for="typeClient_1"><i class="bi bi-person-fill icon fa-2x mx-2 text-dark" style="vertical-align: middle"></i>Particulier</label>
    </div>
    <div class="q-box__question my-5">
        <input class="form-check-input question__input" id="typeClient_2" name="client_type" type="radio" value="corporate">
        <label class="form-check-label question__label_typeClient" for="typeClient_2"><i class="bi bi-briefcase-fill icon  fs-1 mx-3 text-dark" style="vertical-align: middle"></i>Entreprise</label>
    </div>
</div>


