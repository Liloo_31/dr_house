<div class="step2">
    <div class="mt-1 require infoClient">
        <label class="form-label required labelClient">Nom:</label>
        <input class="form-control" id="first_name" required="required" name="first_name" type="text">
    </div>
    <div class="mt-2 infoClient">
        <label class="form-label labelClient">Prénom:</label>
        <input class="form-control" id="last_name" name="last_name" type="text">
    </div>
    <div class="row mt-2 birth">
        <div class="col-lg-5 require infoClient">
            <label class="form-label required labelClient">Date de naissance:</label>
            <input class="form-control" id="birthday" required="required" name="birthday" type="date">
        </div>
        <div class="col-lg-7 require infoClient">
            <label class="form-label required labelClient">Lieu de naissance:</label>
            <input class="form-control" id="place_of_birth" required="required" name="place_of_birth" type="text">
        </div>
    </div>
    <div class="mt-2 require infoClient">
        <label class="form-label required labelClient">Email:</label>
        <input class="form-control" id="email" name="email" required="required" type="text">
    </div>
    <div class="row mt-2">
        <div class="col-lg-6 require infoClient">
            <label class="form-label required labelClient">Téléphone:</label>
            <input class="form-control" id="phone" required="required" name="phone" type="text">
        </div>
        <div class="col-lg-6 require infoClient">
            <label class="form-label required labelClient">Code postal:</label>
            <input class="form-control" id="zip" required="required" name="zip" type="text">
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-lg-6 require infoClient">
            <label class="form-label required labelClient">Adresse:</label>
            <input class="form-control" id="address" required="required" name="address" type="text">
        </div>
        <div class="col-lg-6 infoClient">
            <label class="form-label labelClient">Ville:</label>
            <input class="form-control" id="town" name="city" type="text">
        </div>
    </div>
    <div class="entreprise" style="display: none">
        <div class="row mt-2">
            <div class="col-lg-6 require infoClient">
                <label class="form-label required labelClient">Raison sociale:</label>
                <input class="form-control req" id="company_name" name="company_name" type="text">
            </div>
            <div class="col-lg-6 require infoClient">
                <label class="form-label required labelClient">Numero siret:</label>
                <input class="form-control req" id="siret" name="siret" type="text">
            </div>
        </div>
    </div>
</div>


