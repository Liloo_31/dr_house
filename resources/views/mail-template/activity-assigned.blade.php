@component('mail::message')
# {{ $event ?? '' }}
{{ $causer->name ?? 'on' }} a mis à jour le devis 
Thanks,<br>
{{ app_setting('app_name') }}
@endcomponent
