@component('mail::message')
# {{ $event ?? '' }}
{{ $causer->name ?? 'on' }} a relancé {{ $relaunch->subject->description ?? ''}} sur votre projet <br>
{{ $relaunch->note ?? '' }}
@component('mail::button', ['url' => url("project/detail/$project->id")])
Project detail
@endcomponent
Thanks,<br>
{{ app_setting('app_name') }}
@endcomponent
