<div class="d-flex align-items-center">
    <div class="symbol symbol-45px me-5">
        <img src="{{ $user->avatar_url }}" alt=""/>
    </div>
    <div class="d-flex justify-content-start flex-column">
        <a href="#" class="text-dark fw-bolder text-hover-primary fs-6">{{ $user->name }}</a>

        <span class="text-muted fw-bold text-muted d-block fs-7">{{ $user->type->description }}</span>
    </div>
</div>