<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->longText("content");
            $table->foreignId("sender_id");
            $table->foreignId("project_id")->nullable();
            $table->foreignId("group_id")->nullable();
            $table->foreignId("receiver_id")->nullable();
            $table->longText("files")->nullable();
            $table->string("seen_by")->default(0);
            $table->string("deleted_by")->default(0);
            $table->boolean("deleted")->default(0);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
