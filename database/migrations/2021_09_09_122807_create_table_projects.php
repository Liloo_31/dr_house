<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->foreignId("client_id");
            $table->foreignId("priority_id")->default(1);
            $table->foreignId("status_id")->default(1);
            $table->decimal("price")->default(0.00);
            $table->enum("estimate" ,['accepted', 'refused'])->nullable();
            $table->string("estimate_price")->nullable();
            $table->integer("validation")->nullable();
            // $table->enum("" ,['accepted', 'refused'])->nullable();
            $table->enum("version",["APS" , "DC"])->default("APS");
            $table->integer("correction")->nullable()->default(0);
            $table->boolean("town_planning_study")->default(0);
            $table->boolean("deleted")->default(0);
            $table->date("start_date")->nullable();
            $table->date("due_date")->nullable();
            $table->longText("remark")->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
