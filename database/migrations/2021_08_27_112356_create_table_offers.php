<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->longText("description")->nullable();
            $table->boolean("active")->default(1);
            $table->boolean("deleted")->default(0);
            $table->timestamps();
        });
        $this->seed();
    }

    private function seed(){
        DB::table('offers')->insert([
                ['name' =>"Nouvelle construction"],
                ['name' =>"Travaux sur batîment existant"],
                ['name' =>"Annexe"],
                ['name' =>"Aménagement intérieur en 3D"],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
