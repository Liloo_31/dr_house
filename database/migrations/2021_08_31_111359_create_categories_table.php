<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("description")->nullable();
            $table->boolean("active")->default(1);
            $table->boolean("estimate")->nullable();
            $table->foreignId("offer_id");
            $table->boolean("deleted")->default(0);
            $table->timestamps();
        });
        $this->seed();
    }

    private function seed(){
        DB::table('categories')->insert([
                ['offer_id' => 1, 'name' =>"Maison individuelle de moins de 150m²"],
                ['offer_id' => 1, 'name' =>"Maison individuelle de plus de 150m²"],
                ['offer_id' => 1, 'name' =>"Batiment agricole - Batiment industriel"],
                ['offer_id' => 1, 'name' =>"Logement collectif"],
                ['offer_id' => 1, 'name' =>"Locaux commerciaux bruts"],
                ['offer_id' => 1, 'name' =>"Locaux commerciaux avec aménagements (ERP)"],

                ['offer_id' => 2, 'name' =>"Extension"],
                ['offer_id' => 2, 'name' =>"Rénovation"],
                ['offer_id' => 2, 'name' =>"Modification de façades"],
                ['offer_id' => 2, 'name' =>"Rehaussement"],
                ['offer_id' => 2, 'name' =>"Changement de destination"],
                ['offer_id' => 2, 'name' =>"Aménagement de combles"],
                ['offer_id' => 2, 'name' =>"Véranda"],

                ['offer_id' => 3, 'name' =>"Abri de jardin"],
                ['offer_id' => 3, 'name' =>"Carport / Garage"],
                ['offer_id' => 3, 'name' =>"Panneaux solaires"],
                ['offer_id' => 3, 'name' =>"Piscine"],
                ['offer_id' => 3, 'name' =>"Portail"],
                ['offer_id' => 3, 'name' =>"Terrasse / Pergola"],
                ['offer_id' => 3, 'name' =>"Paysage & Jardin"],

                ['offer_id' => 4, 'name' =>"Séjour"],
                ['offer_id' => 4, 'name' =>"Cuisine"],
                ['offer_id' => 4, 'name' =>"Salle de bain / Toilette"],
                ['offer_id' => 4, 'name' =>"Suite parental"],
                ['offer_id' => 4, 'name' =>"Dressing"],
                ['offer_id' => 4, 'name' =>"Restaurant"],
                ['offer_id' => 4, 'name' =>"Commerce"],
                ['offer_id' => 4, 'name' =>"Chambre d'hotel"],

            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
