<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateQuestionnaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaires', function (Blueprint $table) {
            $table->id();
            $table->longText("question");
            $table->foreignId("category_id")->nullable();
            $table->foreignId("offer_id")->nullable();
            $table->boolean("preliminary")->default(0);
            $table->boolean("deleted")->default(0);
            $table->timestamps();
        });
        $this->seed();
    }

    private function seed(){
        DB::table('questionnaires')->insert([
                ['question' =>"Quel est la surface de plancher crééer par le projet ?", 'preliminary'=>1],
                ['question' =>"Quel est la surface de plancher des constructions existantes ?" , 'preliminary'=>1],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaires');
    }
}
