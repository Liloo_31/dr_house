<?php

namespace App\Jobs;

use App\Models\Project;
use App\Models\User;
use App\Notifications\RelaunchAssignedNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\File;

class RelaunchAssignedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $project;
    protected $created_by;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Project $project ,User $created_by)
    {
        $this->project = $project;
        $this->created_by = $created_by;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->project->client->user;
        $this->project->load("relaunchs");
        $relaunch = $this->project->relaunchs->first();
        $relaunch->load("subject");
        $user->notify(new RelaunchAssignedNotification($this->project, $this->created_by, $relaunch));
    }
}
