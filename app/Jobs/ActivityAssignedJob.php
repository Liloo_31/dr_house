<?php

namespace App\Jobs;

use App\Models\User;
use App\Notifications\ActivityAssignedNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ActivityAssignedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $created_by;
    protected $members;
    protected $actvity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($created_by,$members = [],$activity)
    {
        $this->created_by = $created_by;
        $this->members = $members;
        $this->activity = $activity;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $members = User::findMany($this->ids);
        \Notification::send($members, (new ActivityAssignedNotification($this->activity, $this->created_by)));
    }
}
