<?php

namespace App\Notifications;

use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RelaunchAssignedNotification extends Notification
{
    use Queueable;
    private $project;
    private $causer;
    private $relaunch;
    private $files;
    // private $classification = "bell";
    private $event = "relaunch_assigned";

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $project ,$causer = null, $relaunch = null)
    {
        $this->project = $project;
        $this->causer = $causer;
        $this->relaunch = $relaunch;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage);
        $message->from(app_setting("sender_mail") ,app_setting("sender_name"));
        $message->subject($this->event);      
        $message->markdown('mail-template.relaunch-assigned',["event" => $this->event , "project" => $this->project ,"causer" => $this->causer, "relaunch" => $this->relaunch ]);
        if($this->relaunch->attachements){
            foreach($this->relaunch->attachements as $file){
                $message->attach(relaunch_path_file($file),[
                    'as' => $file->originale_name
                ]);
            }
        }
        return $message;
    }
    

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "project_id" => $this->project->id,
            "event" => $this->event,
            "created_by" => $this->causer->id ?? null,
        ]; 
    }
}
