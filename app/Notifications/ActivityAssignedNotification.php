<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ActivityAssignedNotification extends Notification
{
    use Queueable;
    private $activity;
    private $causer;
    private $event = "activity_assigned";

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($activity,$causer = null)
    {
        $this->activity = $activity;
        $this->causer = $causer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->from(app_setting("sender_mail") ,app_setting("sender_name"))
        ->markdown('mail-template.activity-assigned',["event" => $this->event , "activity" => $this->activity ,"causer" => $this->causer ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
